# TDD Project

C++ project template for TDD.

## Requirements

Requires `cmake 3.21.5`, `pip3`, `conan 1.45.0`.

## Configuration

This section provides configuration details for running the project using Clion & WSL.

### Using WSL toolchains

1. In CLion, go to Settings / Preferences | Build, Execution, Deployment | Toolchains.
2. Click `+` icon to create a new toolchain and select WSL.
3. In the Environment field, select one of the available WSL distributions. The list includes the distributions detected by `wsl.exe --list`, which includes the imported ones.

    ![Screenshot of the add new toolchain window](./assets/toolchains-add-new.png)

4. Wait for all the tools to be detected and save the settings.
5. Set the WSL toolchain as default (move it to the top of the list) or create an associated CMake profile. Use that profile for build, run, and debug.

    ![Screenshot of the toolchains window after adding WSL](./assets/toolchains-wsl-default.png)

More information can be found [here](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-product.html#wsl-tooclhain).

### Using CMake Conan

1. In CLion, go to Settings / Preferences | Advanced Settings.
2. Check the box for `Execute commands in login (-l) shell` under the `WSL toolchain` section.

    ![Screenshot of the Advanced settings Window](./assets/advanced-settings.png)

More information about CMake Conan can be found [here](https://github.com/conan-io/cmake-conan).

### Configuring tests

1. Click on the `configuration` drop down menu in the top right of the main window and chose `Edit Configurations...`.

    ![Screenshot of editing configuration](./assets/configurations-edit.png)

2. Click on the `+` button at the top left of the new window to add a new configuration. Chose `Catch` from the drop down screen.

    ![Screenshot of adding new configuration](./assets/configurations-add-catch.png)

3. Set the Target of the project to `tests` and press `OK`.

    ![Screenshot of setting target](./assets/configurations-target.png)

### Running tests

1. Make sure that the `Catch` configuration is set in the configuration drop down menu and press the `Run` button or press `Shift + F10` to run tests.

    ![Screenshot of running tests](./assets/run-all-tests.png)

2. The test results are made visible in the `Run` pane in the lower part of the main window.

    ![Screenshot of test results](./assets/run-all-tests-results.png)


## Dependencies

This section describes the steps to take in order to install the required dependencies for the project to run.

### CMake

#### Install dependencies

Update and install dependencies
```bash
sudo apt update
sudo apt install build-essential libssl-dev
````

#### Download & extract source

Download and extract CMake source.

Cmake downloads can be found on the official download page [here](https://cmake.org/download/).
At the time of writing, version `3.21.5` was the latest version supported by Clion version `2021.3.3`.

```bash
cd /tmp
wget https://github.com/Kitware/CMake/releases/download/v3.21.5/cmake-3.21.5.tar.gz
tar -zxvf cmake-3.21.5.tar.gz
```

#### Install from source

Install CMake.
```bash
cd cmake-3.21.5
./bootstrap
make
sudo make install
```

Create a symbolic link.
```bash
sudo ln -s /usr/local/bin/cmake /usr/bin/cmake
```

Check that CMake has been installed successfully.
```bash
cmake --version
# cmake version 3.21.5

# CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

### Conan

Install [Conan.io](https://conan.io/) to manage packages.

Conan requires `pip` to be installed as well as `python` version `3.5` and above. Further installation documentation can be found [here](https://docs.conan.io/en/latest/installation.html).

#### Install pip

If `pip` is not already installed, you can install it by running:

```bash
sudo apt update
sudo apt install python3-pip
```

Check that `pip` is installed by running:

```bash
pip3 --version
# pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
```

#### Install Conan

With `pip` installed, install Conan by running the following command:

```bash
pip install conan
```

Check that `conan` is installed by running:

```bash
conan --version
# Conan version 1.45.0
```

## Maintainers

Nizar ([@nizos](https://gitlab.com/nizos))
